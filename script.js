"use strict";

const books = [{
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

function createListFromArray(array) {
    const ul = document.createElement("ul");

    const listOfMissingElements = ['author', 'name', 'price'];

    array.forEach((item, index) => {
        let keys = Object.keys(item); // Масив властивостей даного об'єкта

        // Перевірка, яких властивостей бракує
        function findMissingElements(missingElements, array) {
            return missingElements.filter(item => !array.includes(item));
        }

        let missingElements = findMissingElements(listOfMissingElements, keys);

        try {
            if (missingElements.length !== 0) {
                throw new Error(`В елементі ${index} бракує властивостi/-ей ${missingElements.join(", ")}`);
            }
            const li = document.createElement("li");
            li.textContent = `Автор: ${item.author}. Назва: ${item.name}. Ціна: ${item.price}`;
            ul.appendChild(li);

        } catch (error) {
            console.log(error.message);
        }
    });

    return ul;
}

const ulList = createListFromArray(books);
document.querySelector('#root').appendChild(ulList);